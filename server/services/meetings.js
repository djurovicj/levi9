const mongoose = require('mongoose');
const Meeting = require('../models/meetings');

const getAllMeetings = async () => {

    const meetings = await Meeting.find({}).exec();
    return meetings;
}

const getMeetingById = async (id) => {

    const meeting = await Meeting.findOne({_id:id}).exec();
    return meeting;
}

const getMeetingByDate = async (day) => {

    const meetings = await Meeting.find({day: day}).exec();
    return meetings;
}

const createNewMeeting = async (title, time, description, participants, day) => {
    
    const newMeeting = new Meeting({
        
        _id: new mongoose.Types.ObjectId(),
        title,
        time, 
        description,
        participants, 
        day
    });

    await newMeeting.save();
    return newMeeting;
}

const deleteMeeting = async (id) => {
    await Meeting.findOneAndDelete({_id:id}).exec();
}

const updateMeeting  = async (
    id,
    newTitle,
    newTime,
    newDescription,
    newParticipants
    ) => {
    const updatedMeeting = await Meeting.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          title: newTitle,
          time: newTime,
          description: newDescription,
          participants: newParticipants,
        },
      },
      { new: true }
    );
  
    return updatedMeeting;
  };
  
module.exports = {
    getAllMeetings,
    getMeetingById,
    getMeetingByDate,
    createNewMeeting,
    deleteMeeting,
    updateMeeting
}
