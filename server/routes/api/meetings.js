const express = require('express');
const meetingsController = require('../../controllers/meetings');

const router = express.Router();

router.get('/', meetingsController.getAllMeetings);
router.get('/:id', meetingsController.getMeetingById);
router.get('/day/:day', meetingsController.getMeetingsByDate);

router.post('/', meetingsController.createNewMeeting);
router.post('/:id', meetingsController.updateMeeting);

router.delete('/:id', meetingsController.deleteMeeting);

module.exports = router;