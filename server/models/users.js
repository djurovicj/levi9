const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    surname: {
        type: mongoose.Schema.Types.String,
        required: true
    }
});

const User = mongoose.model('User', userSchema);

module.exports = User;
