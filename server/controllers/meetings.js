const meetingService = require('../services/meetings');

const getAllMeetings = async (req, res, next) => {

    try {
        const data = await meetingService.getAllMeetings();
        res.status(200).json(data);
    } catch (error) {
        next(error);
    }
};

const getMeetingById = async (req, res, next) => {

    const id = req.params.id;

    try {
        if (id == undefined) {
            const error = new Error("Id is missing");
            error.status = 400;
            throw err;
        }

        const meeting = await meetingService.getMeetingById(id);
        if (meeting == null) {
        res.status(404).json();
        } else {
        res.status(200).json(meeting);
        }
    } catch (error) {
        next(error);
  }
};

const getMeetingsByDate = async (req, res, next) => {

    const day = req.params.day;
  
    try {
      if (day == undefined) {
        const error = new Error("Day is missing");
        error.status = 400;
        throw error;
      }
  
      const meetings = await meetingService.getMeetingsByDay(day);
      if (meetings == null) {
        res.status(404).json();
      } else {
        res.status(200).json(meetings);
      }
    } catch (error) {
      next(error);
    }
};

const createNewMeeting = async (req, res, next) => {

    const { title, description, time, participants, day } = req.body;
  
    try {
      if ((!title || !description || !time, !day)) {
        const error = new Error("Invalid input");
        error.status = 400;
        throw error;
      }
  
      const newMeeting = await meetingService.addNewMeeting(
        title,
        time,
        description,
        participants,
        day
      );
      res.status(201).json(newMeeting);
    } catch (error) {
      next(error);
    }
};
    
const deleteMeeting = async (req, res, next) => {

    const id = req.params.id;
  
    try {
      if (!id) {
        const error = new Error("Invalid input");
        error.status = 400;
        throw error;
      }
  
      const meeting = await meetingService.getMeetingById(id);
      if (!meeting) {
        const error = new Error("Meeting doesn't exist");
        error.status = 404;
        throw error;
      }
  
      await meetingService.deleteMeeting(id);
      res.status(200).json();
    } catch (error) {
      next(error);
    }
};

const updateMeeting = async (req, res, next) => {

    const { id, newTitle, newDescription, newTime, newParticipants } = req.body;
  
    try {
      if (!id) {
        const error = new Error("Invalid input");
        error.status = 400;
        throw error;
      }
  
      const meeting = await meetingService.getMeetingById(id);
  
      if (!meeting) {
        const error = new Error("Meeting does not exist");
        error.status = 404;
        throw error;
      }
  
      const updatedMeeting = await meetingService.updateMeeting(
        id,
        newTitle,
        newDescription,
        newTime,
        newParticipants
      );
  
      if (updatedMeeting) {
        res.status(200).json(updatedMeeting);
      } else {
        const error = new Error("Invalid meeting data");
        error.status = 403;
        throw error;
      }
    } catch (error) {
      next(error);
    }
};


  module.exports = {
      getAllMeetings,
      getMeetingById,
      getMeetingsByDate,
      createNewMeeting,
      deleteMeeting,
      updateMeeting
}

